#!/bin/bash

echo "Initializing IDEA to MISP converter..."

# Show converter logs in the terminal
ln -sf /dev/stdout /misp-converter/idea_to_misp.log

# Start converter
python idea_to_misp.py --path "/idea_files" --url "$URLMISP" --key "$APIKEY" --verbose
