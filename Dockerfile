FROM registry.gitlab.com/protective-h2020-eu/collaborations/core:v2

RUN mkdir /idea_files

RUN pip install watchdog

ADD ./idea_to_misp.py .
ADD ./start.sh .

