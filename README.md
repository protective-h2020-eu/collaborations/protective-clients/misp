# MISP

## Dockerized IDEA to MISP

Clone the repository and enter the folder: 

```
git clone https://gitlab.com/protective-h2020-eu/collaborations/protective-clients/misp.git
cd misp
```
Edit `.env` file with: 
*  The URL and client name of Warden Server instance from where you want to download the events.
*  The URL and API Key to connect to the MISP instance where you want to store the converted events.

Put the certificates to connect to Warden Server under ´./data/keys´ folder. If you don't have the certificates, you will need to send an email to Warden Server administrator requesting them.

Run `docker-compose up -d`

Then after 1 minute you should start receiving the converted events in the MISP instance.


## Connector IDEA to MISP

Connector is placed in **idea_to_misp.py**. The connector loads **IDEA** events form folder and converts them to **MISP core format** and pushes them to **MISP** instance. After the conversion is done, converted events are deleted.

If any bug occurs, please try to start the connector with '--verbose' argument, it will help you to find the bug.

### requirements: 
* **Python3.6**
* **PyMISP** python library
* **Watchdog** python library
* **misp** conversion library

### usage: 
```
  idea_to_misp_protective.py [-h] --path IDEA_PATH --url MISP_URL --key MISP_KEY [--cert MISP_CERT] [--test] 
  [--include] [--daemonize] [--pid PID] [--uid UID] [--gid GID] [--log LOG_DEST] [--verbose]
```
#### compulsory arguments:
```
  --path IDEA_PATH  Path to folder, where the IDEA events will be loaded from
  --url MISP_URL    URL to MISP instance
  --key MISP_KEY    MISP instance API key needed for authentication and for pushing events to MISP instance
```
#### optional arguments:
```
  -h, --help         Shows help message
  --cert            Optional MISP instance certificate
  --test            Add Test Tag to converted MISP event
  --include         Convert IDEA events already placed in the folder too
  --daemonize       Run script as daemon
  --pid PID         Create PID file with this name
  --uid UID         User id to run under
  --gid GID         Group id to run under
  --log LOG_DEST    Destination file of log messages
  --verbose         Set logging level to DEBUG
  --thread THREAD_COUNT
                    Set number of threads, which will send MISP events to
                    MISP instance
```