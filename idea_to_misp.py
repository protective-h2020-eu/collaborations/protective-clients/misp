#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018, CESNET, z. s. p. o.
# Use of this source is governed by an ISC license, see LICENSE file.

import os
import json
import argparse
import signal
import resource
import atexit
import sys
import logging
import threading
import queue
import time

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, FileCreatedEvent
from pymisp import ExpandedPyMISP, MISPEvent

from misp import IdeaToMisp

# flag for the conversion completion in case of exiting the script
conversion_active_flag = False
# when script is about to exit, switch to False
running_flag = True
# list of running threads
thread_list = []

logger = logging.getLogger("idea_to_misp.py")


class NewFileHandler(FileSystemEventHandler):
    """
    File handler for watchdog
    """
    def __init__(self, event_queue, test=False):
        self.converter = IdeaToMisp()
        self.test = test
        self.event_queue = event_queue

    def on_created(self, event):
        """
        Whenever file gets created in the folder, this method will pop.
        Loads IDEA event from newly created file, converts it and pushes it into MISP instance
        :param event: DirCreatedEvent or FileCreatedEvent, only FileCreatedEvent gets handled
        :return: None
        """
        global conversion_active_flag, running_flag
        if running_flag:
            # convert all events, block script from exit, allow after conversion is completed
            conversion_active_flag = True
            # if a file was created (not directory), continue
            if isinstance(event, FileCreatedEvent):
                try:
                    with open(event.src_path) as idea_event_file:
                        idea_data = json.load(idea_event_file)
                        converted_event = self.converter.to_misp(idea_data, self.test)
                        # insert converted event to queue with the original IDEA event ID for logging purposes
                        self.event_queue.put((idea_data['ID'], converted_event))
                        logger.debug("Converted IDEA event {} placed to queue!".format(idea_data['ID']))
                    # remove converted file
                    os.remove(event.src_path)
                except Exception:
                    logger.error("Something went wrong while converting data!", exc_info=True)
            conversion_active_flag = False


def daemonize(uid=None, gid=None, pidfile=None, signals={}):
    """
    Daemonizes the process
    """
    if gid is not None:
        os.setgid(gid)
    if uid is not None:
        os.setuid(uid)
    # Doublefork, split session
    if os.fork() > 0:
        os._exit(0)
    try:
        os.setsid()
    except OSError:
        pass
    if os.fork() > 0:
        os._exit(0)
    # Setup signal handlers
    for (signum, handler) in signals.items():
        signal.signal(signum, handler)
    # Close descriptors
    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if maxfd == resource.RLIM_INFINITY:
        maxfd = 65535
    for fd in range(maxfd, 3, -1):  # 3 means omit stdin, stdout, stderr
        try:
            os.close(fd)
        except Exception:
            pass
    # Redirect stdin, stdout, stderr to /dev/nulla
    devnull = os.open(os.devnull, os.O_RDWR)
    for fd in range(3):
        os.dup2(devnull, fd)
    # PID file
    if pidfile is not None:
        pidd = os.open(pidfile, os.O_RDWR | os.O_CREAT | os.O_EXCL | os.O_TRUNC)
        os.write(pidd, (str(os.getpid())+"\n").encode('utf-8'))
        os.close(pidd)
        # Define and setup atexit closure
        @atexit.register
        def unlink_pid():
            try:
                os.unlink(pidfile)
            except Exception:
                pass


def save_event(misp_inst, event_queue):
    """
    Function for threads, which will pull converted event from the queue and will send it to MISP instance
    :param misp_inst: instance of connection to MISP instance
    :param event_queue: queue of converted events
    :return: None
    """
    global conversion_active_flag, running_flag
    while running_flag or conversion_active_flag or not event_queue.empty():
        if not event_queue.empty():
            # events in event queue are stored as 2-tuple of original IDEA event ID and converted MISP event, which is
            # instance of MISPEvent() --> (idea_event['ID'], converted_event)
            idea_event_id, converted_event = event_queue.get()
            logger.debug("IDEA event {} sent to MISP instance, event queue length: {}".format(idea_event_id,
                                                                                              event_queue.qsize()))
            response = misp_inst.add_event(converted_event)

            if isinstance(response, MISPEvent):
                logger.info("IDEA event {idea_id} was successfully converted and added to MISP instance under "
                            "id {misp_id}!".format(idea_id=idea_event_id, misp_id=response.id))
            elif isinstance(response, dict) and not response.get('errors'):
                logger.info("IDEA event {idea_id} was successfully converted and added to MISP instance under "
                            "id {misp_id}!".format(idea_id=idea_event_id, misp_id=response['Event']['id']))
            else:
                try:
                    if response['errors'][0][1]['name'].startswith("Event already exists"):
                        logger.error("IDEA event {idea_id} was not converted, because original MISP event, "
                                     "from which it was converted, already exists in the MISP instance!".format(
                                     idea_id=idea_event_id))
                    else:
                        # all other response errors are handled in KeyError exception
                        raise Exception
                except Exception:
                    logger.error("IDEA event {idea_id} was not pushed to MISP instance, because something went wrong "
                                 "MISP server side!".format(idea_id=idea_event_id))
        else:
            # if no events in the queue, thread can sleep for 0.5 seconds
            time.sleep(0.5)


def get_args():
    """
    Create argument parser and initialize all possibly used arguments
    :return: argument parser
    """
    parser = argparse.ArgumentParser(
        description="Converter script, which loads IDEA events and converts them to MISP core format and pushes them "
                    "to MISP instance.")
    parser.add_argument("--path", required=True, dest="idea_path", action="store",
                        help="Path to folder, where the IDEA events will be loaded from")
    parser.add_argument("--url", required=True, dest="misp_url", action="store",
                        help="URL to MISP instance")
    parser.add_argument("--key", required=True, dest="misp_key", action="store",
                        help="MISP instance API key needed for authentication and for pushing events to MISP instance")
    parser.add_argument("--cert", required=False, dest="misp_cert", action="store", default=None,
                        help="Path to optional MISP instance certificate")
    parser.add_argument("--test", required=False, dest="test", action="store_true", default=False,
                        help="Add Test Tag to converted MISP event")
    parser.add_argument("--include", required=False, dest="include_dir", action="store_true", default=False,
                        help="Convert IDEA events already placed in the folder too")
    parser.add_argument("--daemonize", required=False, dest="daemonize", action="store_true", default=False,
                        help="Run script as daemon")
    parser.add_argument("--pid",
                        default=os.path.join("/var/run", os.path.splitext(os.path.basename(sys.argv[0]))[0] + ".pid"),
                        dest="pid", action="store", help="Create PID file with this name"),
    parser.add_argument("--uid", default=None, dest="uid", action="store", help="User id to run under")
    parser.add_argument("--gid", default=None, dest="gid", action="store", help="Group id to run under")
    parser.add_argument("--log", dest="log_dest", action="store", default="idea_to_misp.log",
                        help="Destination file of log messages")
    parser.add_argument("--verbose", required=False, dest="verbose", default=False, action="store_true",
                        help="Set logging level to DEBUG")
    parser.add_argument("--thread", required=False, dest="thread_count", default=4, action="store",
                        help="Set number of threads, which will send MISP events to MISP instance")
    return parser


def terminate_me(signum, frame):
    """
    Sets running flag to fals and terminates process, waits until any conversion finishes
    :return: None
    """
    global conversion_active_flag, running_flag, thread_list
    running_flag = False
    while True:
        if not conversion_active_flag:
            logger.debug("Waiting for all threads to stop!")
            for thread in thread_list:
                thread.join()
            logger.info("Exiting the connector!")
            sys.exit()


def main():
    # parse arguments
    arguments = get_args().parse_args()

    logging_level = logging.DEBUG if arguments.verbose else logging.INFO

    # configure logger
    logging.basicConfig(filename=arguments.log_dest, filemode='w',
                        format='%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', level=logging_level)

    # initialize connection to MISP instance
    try:
        misp_inst = ExpandedPyMISP(url=arguments.misp_url, key=arguments.misp_key, cert=arguments.misp_cert)
    except Exception:
        logger.error("Cannot connect to MISP instance {url}!".format(url=arguments.misp_url), exc_info=True)
        sys.exit()

    if arguments.daemonize:
        try:
            daemonize(
                pidfile=arguments.pid,
                uid=arguments.uid,
                gid=arguments.gid,
                signals={
                    signal.SIGINT: terminate_me,
                    signal.SIGTERM: terminate_me,
                })
        except Exception:
            logger.error("Something went wrong while trying to daemonize!", exc_info=True)
            sys.exit()
    else:
        signal.signal(signal.SIGINT, terminate_me)
        signal.signal(signal.SIGTERM, terminate_me)

    # queue of converted events, which are waiting for send to MISP instance
    event_queue = queue.Queue(maxsize=30)
    global thread_list
    # init Threads, which will pull converted events from queue and send them to MISP instance
    for i in range(arguments.thread_count):
        t = threading.Thread(target=save_event, args=(misp_inst, event_queue))
        thread_list.append(t)
        t.start()

    # process IDEA events, which were created earlier
    if arguments.include_dir:
        global conversion_active_flag, running_flag
        # load all of the filenames
        all_files = os.listdir(arguments.idea_path)
        converter = IdeaToMisp()
        # process all of the files
        for idea_filename in all_files:
            if running_flag:
                conversion_active_flag = True
                try:
                    with open(os.path.join(arguments.idea_path, idea_filename)) as idea_file:
                        idea_data = json.load(idea_file)
                        converted_event = converter.to_misp(idea_data, arguments.test)
                        event_queue.put((idea_data['ID'], converted_event))
                        # remove file after conversion
                        os.remove(os.path.join(arguments.idea_path, idea_filename))
                except Exception:
                    logger.error("Something went wrong while converting IDEA event!", exc_info=True)
                conversion_active_flag = False

    # Initialize and start File watchdog, pops whenever new IDEA message is created in the directory
    observer = Observer()
    event_handler = NewFileHandler(event_queue, arguments.test)
    observer.schedule(event_handler, arguments.idea_path, recursive=False)
    try:
        observer.start()
    except OSError:
        logger.error("Path is not a directory!", exc_info=True)
        sys.exit()
    observer.join()


if __name__ == "__main__":
    main()
